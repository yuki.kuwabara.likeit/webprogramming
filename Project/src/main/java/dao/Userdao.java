package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Userdao {

  // LoginServlet
  public User findByLoginInfo(String loginId, String password) {

    Connection conn = null;

    try {
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);

      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      } else {

      String loginIdData = rs.getString("login_id");
      String nameData = rs.getString("name");
      return new User(loginIdData, nameData);
    }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;

    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // UserListServlet
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user  WHERE login_id != 'admin' ";

      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");

        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;

    } finally {
      if (conn != null) {
        try {
          conn.close();

        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  // add
  public void createUser(String loginId, String password, String name, String birthDate) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql =
          "INSERT INTO user(login_id, password, name, birth_date, create_date, update_date) VALUES(?,?,?,?,now(),now())";
      PreparedStatement pStmt = conn.prepareStatement(sql);

      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      pStmt.setString(3, name);
      pStmt.setString(4, birthDate);

      pStmt.executeUpdate();
      
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // details
  public User findById(int id) { // 命名を変更する// 動詞から！
   Connection conn = null;
   User user = null;

   try {
   conn = DBManager.getConnection();
   String sql =
       "SELECT * FROM user WHERE id = ? ";

   PreparedStatement pStmt = conn.prepareStatement(sql);
   pStmt.setInt(1, id);

   ResultSet rs = pStmt.executeQuery();
   
   while (rs.next()) {
     String loginId = rs.getString("login_id");
     String name = rs.getString("name");
     Date birthDate = rs.getDate("birth_date");
     String password = rs.getString("password");
     boolean isAdmin = rs.getBoolean("is_admin");
     Timestamp createDate = rs.getTimestamp("create_date");
     Timestamp updateDate = rs.getTimestamp("update_date");
  
     user = new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);
   }
   } catch (SQLException e) {
     e.printStackTrace();
     return null;

   } finally {
     if (conn != null) {
       try {
         conn.close();

       } catch (SQLException e) {
         e.printStackTrace();
         return null;
       }
     }
   }
   return user;
 }

 public void update(String password, String name, String birthDate, String id) {
   Connection conn = null;

   try {
     conn = DBManager.getConnection();
     String sql =
         "UPDATE user SET  password = ?,name =?,birth_date =?,update_date = now() WHERE id =?;";
     PreparedStatement pStmt = conn.prepareStatement(sql);

     pStmt.setString(1, password);
     pStmt.setString(2, name);
     pStmt.setString(3, birthDate);
     // pStmt.setString(4, updateDate);
     pStmt.setString(4, id);

     pStmt.executeUpdate();

   } catch (SQLException e) {
     e.printStackTrace();
     return;
   } finally {
     if (conn != null) {
       try {
         conn.close();
       } catch (SQLException e) {
         e.printStackTrace();
         return;
       }
     }
   }
   return;
 }

 public User delete(int id) {
   Connection conn = null;

   try {
     conn = DBManager.getConnection();
     String sql = "DELETE FROM user WHERE id = ?;";
     PreparedStatement pStmt = conn.prepareStatement(sql);

     pStmt.setInt(1, id);
     pStmt.executeUpdate();

   } catch (SQLException e) {
     e.printStackTrace();
     return null;

   } finally {
     if (conn != null) {
       try {
         conn.close();
       } catch (SQLException e) {
         e.printStackTrace();
         return null;
       }
     }
   }
   return null;
}

public boolean exist(String loginId) {
  Connection conn = null;

  try {
    conn = DBManager.getConnection();
    String sql = "SELECT * FROM user WHERE login_id = ? ";
    PreparedStatement pStmt = conn.prepareStatement(sql);

    pStmt.setString(1, loginId);

    ResultSet rs = pStmt.executeQuery();

    if (rs.next()) {
      return true;
    }
    return false;
  } catch (SQLException e) {
    e.printStackTrace();
    return false;

  } finally {
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
        return false;
      }
    }
}
}

public List<User> select(String loginId, String name, String startDate, String endDate) {
  Connection conn = null;
  List<User> user = new ArrayList<User>();

  String sql = "SELECT * FROM user WHERE login_id != 'admin' ";
  StringBuilder str = new StringBuilder(sql);

  List<String> list = new ArrayList<String>();

  if (loginId != null && !(loginId.equals(""))) {
    str.append("and login_id = ?");
    list.add(loginId);
      }

      if (name != null && !(name.equals(""))) {
        str.append("and name like ?");
    list.add("%" + name + "%");
  }

  if (startDate != null && !(startDate.equals(""))) {
    str.append("and birth_date >= ? ");
    list.add(startDate);
  }

    if (endDate != null  && !(endDate.equals(""))) { 
      str.append("and birth_date <= ? ");
      list.add(endDate);
  }

  try {
    conn = DBManager.getConnection();
    PreparedStatement pstmt = conn.prepareStatement(str.toString());

    System.out.println(loginId + name + startDate + endDate);

    for (int i = 0; i < list.size(); i++) {
      pstmt.setString(i + 1, list.get(i));
    }

    ResultSet rs = pstmt.executeQuery();

    while (rs.next()) {
      int id = rs.getInt("id");
      String login = rs.getString("login_id");
      String nm = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");

      User userList = new User(id, login, nm, birthDate, password, isAdmin, createDate, updateDate);
      user.add(userList);
    }

  } catch (SQLException e) {
    e.printStackTrace();
    return null;

  } finally {
    if (conn != null) {
      try {
        conn.close();

      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      }
    }
  }
  return user;
}
}
