package contoller.controller;


import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.User;
import dao.Userdao;
import util.PasswordEncorder;

/**
 * Servlet implementation class Login
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
	  dispatcher.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    request.setCharacterEncoding("UTF-8");
	    
        String loginId = request.getParameter("loginid");
        String password = request.getParameter("password");
        System.out.println("loginid:" + loginId + " password:" + password);

        Userdao userDao = new Userdao();

        String pass = PasswordEncorder.encordPassword(password);
        // return;
        User user = userDao.findByLoginInfo(loginId, pass);

        if (user == null) {
          request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
          request.setAttribute("loginId", loginId);
          
          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
              dispatcher.forward(request,response);
              return;

        }

        HttpSession session = request.getSession();
        session.setAttribute("userInfo", user);
        
        response.sendRedirect("UserListServlet");
}
}
