package contoller.controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.User;
import dao.Userdao;
import util.PasswordEncorder;

/**
 * Servlet implementation class userUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      int id = Integer.parseInt(request.getParameter("id"));
      HttpSession session = request.getSession();
      User u = (User) session.getAttribute("userInfo");

      Userdao userDao = new Userdao();
      User user = userDao.findById(id);

      request.setAttribute("user", user);

      if (u != null) {
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request,response);
      return;
    } else {
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
      dispatcher.forward(request, response);
      return;
    }
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      String id = request.getParameter("user-id");
      String password = request.getParameter("password");
      String confirm = request.getParameter("password-confirm");
      String name = request.getParameter("user-name");
      String birthDate = request.getParameter("birth-date");
      
      Userdao userDao = new Userdao();

      try {
        User user = new User();
        Date format = Date.valueOf(birthDate);

      // チェックする
      if (!(password.equals(confirm)) || name.equals("")
          || birthDate.equals("")) {
        request.setAttribute("errMsg", "入力された内容は正しくありません");
        user.setLoginId(id);
        user.setName(name);
        user.setBirthDate(format);

        request.setAttribute("user", user);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;

       }
       String pass = PasswordEncorder.encordPassword(password);
       // return;

       userDao.update(pass, name, birthDate, id);
      response.sendRedirect("UserListServlet");
    } catch (Exception e) {
      e.printStackTrace();
    }
}
}
