package contoller.controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.User;
import dao.Userdao;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();
      User u = (User) session.getAttribute("userInfo");

      if (u != null) {
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
    } else {
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
      dispatcher.forward(request, response);
      return;
      }
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      
      String loginId = request.getParameter("user-loginid");
      String password = request.getParameter("password");
      String confirm = request.getParameter("password-confirm");
      String name = request.getParameter("user-name");
      String birthDate = request.getParameter("birth-date");
      System.out.println(
          "loginid:" + loginId + " password:" + password + "name:" + name + "生年月日:" + birthDate);

      Userdao userDao = new Userdao();
      boolean existUser = userDao.exist(loginId);
      Date _birthDate = null;

      // User user = new User();
      if (!(birthDate.equals(""))) {
        _birthDate = Date.valueOf(birthDate);

      }
        if (existUser) {
        request.setAttribute("errMsg", "入力された内容は正しくありません");
        request.setAttribute("loginId", loginId);
        request.setAttribute("name", name);
        request.setAttribute("birthDate", _birthDate);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;

      } else if (!(password.equals(confirm)) || password.equals("") || name.equals("")
          || birthDate.equals("") || loginId.equals("")) {
        request.setAttribute("errMsg", "入力された内容は正しくありません");
        request.setAttribute("loginId", loginId);
        request.setAttribute("name", name);
        request.setAttribute("birthDate", _birthDate);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }

      String pass = PasswordEncorder.encordPassword(password);
      // return;
      userDao.createUser(loginId, pass, name, birthDate);

      response.sendRedirect("UserListServlet");
}
}
