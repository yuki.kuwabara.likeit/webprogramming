package contoller.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.User;
import dao.Userdao;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
      super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // URLからGETパラメータとしてIDを受け取る
      int id = Integer.parseInt(request.getParameter("id"));

      // 確認用：idをコンソールに出力
      System.out.println(id);

      // TODO 未実装：idを引数にして、idに紐づくユーザ情報を出力する
      Userdao userDao = new Userdao();
      User user = userDao.findById(id);

      request.setAttribute("user", user);

      // TODO 未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
      // if (user.equals("")) {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
        dispatcher.forward(request, response);
        // } else
        // response.sendRedirect("LoginServlet");

      }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

    }

}
