package contoller.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.User;
import dao.Userdao;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();

        // TODO Auto-generated constructor stub
    }

	/**
     * 
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      Userdao userDao = new Userdao();
      List<User> userList = userDao.findAll();
      HttpSession session = request.getSession();
      User u = (User) session.getAttribute("userInfo");

      if (u != null) {
      request.setAttribute("userList", userList);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);
	}
    else {
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
      dispatcher.forward(request, response);
      return;
    }
  }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      String loginId = request.getParameter("user-loginid");
      String name = request.getParameter("user-name");
      String startDate = request.getParameter("date-start");
      String endDate = request.getParameter("date-end");

      Userdao userDao = new Userdao();
      List<User> user = userDao.select(loginId, name, startDate, endDate);

      request.setAttribute("userList", user);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
        dispatcher.forward(request, response);
      }
    }




