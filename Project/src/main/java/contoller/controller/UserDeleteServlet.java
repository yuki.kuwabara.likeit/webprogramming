package contoller.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.User;
import dao.Userdao;

/**
 * Servlet implementation class userDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      int id = Integer.parseInt(request.getParameter("id"));
      HttpSession session = request.getSession();
      User u = (User) session.getAttribute("userInfo");
      Userdao userDao = new Userdao();
      User user = userDao.findById(id);

      request.setAttribute("user", user);

      if (u == null) {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
        dispatcher.forward(request, response);
        return;

      }
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
      dispatcher.forward(request, response);
      return;
    }



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      int id = Integer.parseInt(request.getParameter("user-id"));

      Userdao userDao = new Userdao();
      User user = userDao.delete(id);

      request.setAttribute("user", user);


      // RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
      // dispatcher.forward(request, response);

      response.sendRedirect("UserListServlet");
	}

}
