<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>ContentFilters</key>
	<dict/>
	<key>auto_connect</key>
	<true/>
	<key>data</key>
	<dict>
		<key>connection</key>
		<dict>
			<key>database</key>
			<string>usermanagement</string>
			<key>host</key>
			<string>127.0.0.1</string>
			<key>kcid</key>
			<string>-5295504636938905206</string>
			<key>name</key>
			<string>127.0.0.1</string>
			<key>rdbms_type</key>
			<string>mysql</string>
			<key>sslCACertFileLocation</key>
			<string></string>
			<key>sslCACertFileLocationEnabled</key>
			<integer>0</integer>
			<key>sslCertificateFileLocation</key>
			<string></string>
			<key>sslCertificateFileLocationEnabled</key>
			<integer>0</integer>
			<key>sslKeyFileLocation</key>
			<string></string>
			<key>sslKeyFileLocationEnabled</key>
			<integer>0</integer>
			<key>type</key>
			<string>SPTCPIPConnection</string>
			<key>useSSL</key>
			<integer>0</integer>
			<key>user</key>
			<string>root</string>
		</dict>
		<key>session</key>
		<dict>
			<key>connectionEncoding</key>
			<string>utf8</string>
			<key>contentFilter</key>
			<dict/>
			<key>contentPageNumber</key>
			<integer>1</integer>
			<key>contentSelection</key>
			<data>
			YnBsaXN0MDDUAQIDBAUGBwpYJHZlcnNpb25ZJGFyY2hpdmVyVCR0
			b3BYJG9iamVjdHMSAAGGoF8QD05TS2V5ZWRBcmNoaXZlctEICVRk
			YXRhgAGtCwwZGhscHSEoLDA1OlUkbnVsbNMNDg8QFBhXTlMua2V5
			c1pOUy5vYmplY3RzViRjbGFzc6MREhOAAoADgASjFRYXgAWABoAI
			gAxUdHlwZVRyb3dzVGtleXNfECZTZWxlY3Rpb25EZXRhaWxUeXBl
			UHJpbWFyeUtleWVkRGV0YWlsc9MNDg8eHyCgoIAH0iIjJCVaJGNs
			YXNzbmFtZVgkY2xhc3Nlc18QE05TTXV0YWJsZURpY3Rpb25hcnmj
			JCYnXE5TRGljdGlvbmFyeVhOU09iamVjdNIODykroSqACYAL0g8t
			Li9ZTlMuc3RyaW5ngApSaWTSIiMxMl8QD05TTXV0YWJsZVN0cmlu
			Z6MxMzRYTlNTdHJpbmdYTlNPYmplY3TSIiM2N15OU011dGFibGVB
			cnJheaM2ODlXTlNBcnJheVhOU09iamVjdNIiIyY7oiY8WE5TT2Jq
			ZWN0AAgAEQAaACQAKQAyADcASQBMAFEAUwBhAGcAbgB2AIEAiACM
			AI4AkACSAJYAmACaAJwAngCjAKgArQDWAN0A3gDfAOEA5gDxAPoB
			EAEUASEBKgEvATEBMwE1AToBRAFGAUkBTgFgAWQBbQF2AXsBigGO
			AZYBnwGkAacAAAAAAAACAQAAAAAAAAA9AAAAAAAAAAAAAAAAAAAB
			sA==
			</data>
			<key>contentSortColIsAsc</key>
			<true/>
			<key>contentViewport</key>
			<string>{{0, 0}, {734, 469}}</string>
			<key>isToolbarVisible</key>
			<true/>
			<key>queries</key>
			<string>CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

CREATE TABLE user(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
is_admin boolean NOT NULL default false,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL 
);

INSERT INTO user(
login_id,
name,
birth_date,
password,
is_admin,
create_date,
update_date
)
VALUES(
'admin',
'管理者',
'2022/01/20',
'password',
false,
now(),
now()
);
</string>
			<key>table</key>
			<string>user</string>
			<key>view</key>
			<string>SP_VIEW_CUSTOMQUERY</string>
			<key>windowVerticalDividerPosition</key>
			<real>214</real>
		</dict>
	</dict>
	<key>encrypted</key>
	<false/>
	<key>format</key>
	<string>connection</string>
	<key>queryFavorites</key>
	<array/>
	<key>queryHistory</key>
	<array>
		<string>CREATE TABLE user(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
is_admin boolean NOT NULL default false,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL 
);
INSERT INTO user(
login_id,
name,
birth_date,
password,
is_admin,
create_date,
update_date
)
VALUES(
'admin',
'管理者',
'2022/01/20',
'password',
false,
now(),
now()
)</string>
		<string>insert into user VALUE(2,'admin01', 'Alex', 19970312, 'alex', 2, now(), now())</string>
		<string>insert into user VALUE(2,'admin01', 'Alex', 1997/03/12, 'alex', 2, now(), now())</string>
		<string>insert into user VALUE(2,'admin01', 'Alex', 1997/03/12, 'alex')</string>
		<string>insert into user VALUE('admin01', 'Alex', 1997/03/12, 'alex')</string>
		<string>insert into user VALUE('admin01', 'Alex', 1997/03/12, 'alex', 2)</string>
		<string>insert into user VALUE('admin01', 'Alex', 1997/03/12, 'alex', 1, now(),now())</string>
		<string>insert into user VALUE('admin01', 'Alex', 1997/03/12, 'alex', 1, now,now)</string>
		<string>insert into user VALUE(admin01, Alex, 1997/03/12, alex, 1, now,now)</string>
		<string>insert into user VALUE(admin01, Alex, 1997/03/12, alex, 1, now(),now())</string>
		<string>INSERT INTO user(
login_id,
name,
birth_date,
password,
is_admin,
create_date,
update_date
)
VALUES(
'admin',
'管理者',
'2022/01/20',
'password',
false,
now(),
now()
)</string>
		<string>INSERT INTO user(
login_id,
name,
birth_date,
password,
is_admin,
create_date,
update_date
)
VALUES(
'admin',
'管理者',
2022/01/20,
password,
false,
now(),
now()
)</string>
		<string>INSERT INTO user(
login_id,
name,
birth_date,
password,
is_admin,
create_date,
update_date
)
VALUES(
admin,
'管理者',
2022/01/20,
password,
false,
now(),
now()
)</string>
		<string>INSERT INTO user(
name,
birth_date,
password,
is_admin,
create_date,
update_date
)
VALUES(
admin,
'管理者',
2022/01/20,
password,
false,
now(),
now()
)</string>
		<string>INSERT INTO user(
login_id,
name,
birth_date,
password,
is_admin,
create_date,
update_date
)
VALUES(
,
admin,
'管理者',
2022/01/20,
password,
false,
now(),
now()
)</string>
		<string>INSERT INTO user(
login_id,
name,
birth_date,
password,
is_admin,
create_date,
update_date
)
VALUES(
"",
admin,
'管理者',
2022/01/20,
password,
false,
now(),
now()
)</string>
		<string>CREATE TABLE user(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
is_admin boolean NOT NULL,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL 
)</string>
		<string>CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
CREATE TABLE user(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
is_admin boolean NOT NULL,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL 
);
INSERT INTO user(
login_id,
name,
birth_date,
password,
is_admin,
create_date,
update_date
)
VALUES(
admin,
'管理者',
2022/01/20,
password,
false,
now(),
now()
)</string>
		<string>select * from textbook_samlpe</string>
		<string>select * from m_cutomer</string>
	</array>
	<key>rdbms_type</key>
	<string>mysql</string>
	<key>rdbms_version</key>
	<string>5.7.22</string>
	<key>version</key>
	<integer>1</integer>
</dict>
</plist>
